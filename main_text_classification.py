import torch
from torch import nn
import numpy as np
from si_models import SILinear, SILSTM
from torchtext import data
from torchtext.vocab import Vectors
import spacy
import pandas as pd
import numpy as np
import torch.optim as optim
import sys
from sklearn.metrics import accuracy_score

# config.py
class Config(object):
    embed_size = 100
    hidden_layers = 1
    hidden_size = 32
    bidirectional = True
    output_size = 4
    max_epochs = 12
    lr = 0.001#0.25
    batch_size = 300
    max_sen_len = 20 # Sequence length for RNN
    dropout_keep = 0.8


class TextSIRNN(nn.Module):
    def __init__(self, config, vocab_size, word_embeddings, noise_std=0.0):
        super(TextSIRNN, self).__init__()
        self.config = config
        self.noise_std = noise_std
        # Embedding Layer
        self.embeddings = nn.Embedding(vocab_size, self.config.embed_size)
        self.embeddings.weight = nn.Parameter(word_embeddings, requires_grad=False)

        self.lstm = SILSTM(input_size=self.config.embed_size,
                                       hidden_size=self.config.hidden_size,
                                       noise_std=self.noise_std,
                                       num_layers=self.config.hidden_layers,
                                       bidirectional=self.config.bidirectional)

        self.dropout = nn.Dropout(self.config.dropout_keep)

        # Fully-Connected Layer
        self.fc = SILinear(
            self.config.hidden_size * self.config.hidden_layers * (1 + self.config.bidirectional),
            self.config.output_size, bias=True, noise_std=self.noise_std
        )

        self.lay_norm = nn.LayerNorm(self.config.output_size, elementwise_affine=False)

        # Softmax non-linearity
        self.logsoftmax = nn.LogSoftmax()

    def forward(self, x, noise_std=0.0):
        # x.shape = (max_sen_len, batch_size)
        embedded_sent = self.embeddings(x)
        # embedded_sent.shape = (max_sen_len=20, batch_size=64,embed_size=300)
        self.noise_std = noise_std
        if self.noise_std > 0:
            # print("I reach here")
            lstm_out, (h_n, c_n) = self.lstm(embedded_sent, noise_std=self.noise_std)
        else:
            lstm_out, (h_n, c_n) = self.lstm(embedded_sent)
        final_feature_map = self.dropout(h_n)  # shape=(num_layers * num_directions, 64, hidden_size)

        # Convert input to (64, hidden_size * hidden_layers * num_directions) for linear layer
        final_feature_map = torch.cat([final_feature_map[i, :, :] for i in range(final_feature_map.shape[0])], dim=1)
        final_out = self.fc(final_feature_map)
        final_out = self.lay_norm(final_out)

        return self.logsoftmax(final_out)

    def add_optimizer(self, optimizer):
        self.optimizer = optimizer

    def add_loss_op(self, loss_op):
        self.loss_op = loss_op

    def reduce_lr(self):
        print("Reducing LR")
        for g in self.optimizer.param_groups:
            g['lr'] = g['lr'] / 2

    def run_epoch(self, train_iterator, val_iterator, epoch):
        train_losses = []
        val_accuracies = []
        losses = []

        # Reduce learning rate as number of epochs increase
        if (epoch == int(self.config.max_epochs / 3)) or (epoch == int(2 * self.config.max_epochs / 3)):
            self.reduce_lr()

        for i, batch in enumerate(train_iterator):
            self.optimizer.zero_grad()
            if torch.cuda.is_available():
                x = batch.text.cuda()
                y = (batch.label - 1).type(torch.cuda.LongTensor)
            else:
                x = batch.text
                y = (batch.label - 1).type(torch.LongTensor)
            y_pred = self.__call__(x)
            loss = self.loss_op(y_pred, y)
            loss.backward()
            losses.append(loss.data.cpu().numpy())
            self.optimizer.step()

            if i % 100 == 0:
                print("Iter: {}".format(i + 1))
                avg_train_loss = np.mean(losses)
                train_losses.append(avg_train_loss)
                print("\tAverage training loss: {:.5f}".format(avg_train_loss))
                losses = []

                # Evalute Accuracy on validation set
                val_accuracy = evaluate_model(self, val_iterator)
                print("\tVal Accuracy: {:.4f}".format(val_accuracy))
                self.train()

        return train_losses, val_accuracies

    def run_epoch_sidp(self, train_iterator, val_iterator, epoch, clip, noise_multiplier, lr, batch_size):
        # sidp
        train_losses = []
        val_accuracies = []
        losses = []

        # Reduce learning rate as number of epochs increase
        if (epoch == int(self.config.max_epochs / 3)) or (epoch == int(2 * self.config.max_epochs / 3)):
            self.reduce_lr()

        sgd_lr = lr
        for g in self.optimizer.param_groups:
            sgd_lr = g['lr']

        dp_std = sgd_lr * clip * noise_multiplier / batch_size

        for i, batch in enumerate(train_iterator):
            self.optimizer.zero_grad()
            if torch.cuda.is_available():
                x = batch.text.cuda()
                y = (batch.label - 1).type(torch.cuda.LongTensor)
            else:
                x = batch.text
                y = (batch.label - 1).type(torch.LongTensor)
            y_pred = self.__call__(x, dp_std)
            loss = self.loss_op(y_pred, y)

            saved_var = dict()
            for tensor_name, tensor in self.named_parameters():
                if tensor_name == "embeddings.weight": continue
                saved_var[tensor_name] = torch.zeros_like(tensor)

            # print(saved_var.keys())

            for j in loss:
                j.backward(retain_graph=True)
                # param_norm_before = compute_norm(model, norm_type=2)
                torch.nn.utils.clip_grad_norm_(self.parameters(), clip, norm_type=2)
                # param_norm_after = compute_norm(model, norm_type=2)
                # total_diff += np.abs(param_norm_after - param_norm_before)
                # print(self.named_parameters())
                for tensor_name, tensor in self.named_parameters():
                    if tensor_name == "embeddings.weight": continue
                    new_grad = tensor.grad
                    saved_var[tensor_name].add_(new_grad)
                self.optimizer.zero_grad()

            for tensor_name, tensor in self.named_parameters():
                if tensor_name == "embeddings.weight": continue
                # saved_var[tensor_name].add_(tensor.grad)
                # noise = std_grad * torch.randn_like(tensor.grad)
                # saved_var[tensor_name].add_(noise)
                tensor.grad = saved_var[tensor_name] / loss.shape[0]

            # loss.backward()
            losses.append(loss.data.mean().cpu().numpy())
            self.optimizer.step()

            if i % 100 == 0:
                print("Iter: {}".format(i + 1))
                avg_train_loss = np.mean(losses)
                train_losses.append(avg_train_loss)
                print("\tAverage training loss: {:.5f}".format(avg_train_loss))
                losses = []

                # Evalute Accuracy on validation set
                val_accuracy = evaluate_model(self, val_iterator)
                print("\tVal Accuracy: {:.4f}".format(val_accuracy))
                self.train()

        return train_losses, val_accuracies


class TextRNN(nn.Module):
    def __init__(self, config, vocab_size, word_embeddings):
        super(TextRNN, self).__init__()
        self.config = config

        # Embedding Layer
        self.embeddings = nn.Embedding(vocab_size, self.config.embed_size)
        self.embeddings.weight = nn.Parameter(word_embeddings, requires_grad=False)

        self.lstm = nn.LSTM(input_size=self.config.embed_size,
                            hidden_size=self.config.hidden_size,
                            num_layers=self.config.hidden_layers,
                            # dropout = self.config.dropout_keep,
                            bidirectional=self.config.bidirectional)

        self.dropout = nn.Dropout(self.config.dropout_keep)

        # Fully-Connected Layer
        self.fc = nn.Linear(
            self.config.hidden_size * self.config.hidden_layers * (1 + self.config.bidirectional),
            self.config.output_size
        )

        # Softmax non-linearity
        self.logsoftmax = nn.LogSoftmax()

    def forward(self, x):
        # x.shape = (max_sen_len, batch_size)
        embedded_sent = self.embeddings(x)
        # embedded_sent.shape = (max_sen_len=20, batch_size=64,embed_size=300)

        lstm_out, (h_n, c_n) = self.lstm(embedded_sent)
        final_feature_map = self.dropout(h_n)  # shape=(num_layers * num_directions, 64, hidden_size)

        # Convert input to (64, hidden_size * hidden_layers * num_directions) for linear layer
        final_feature_map = torch.cat([final_feature_map[i, :, :] for i in range(final_feature_map.shape[0])], dim=1)
        final_out = self.fc(final_feature_map)
        return self.logsoftmax(final_out)

    def add_optimizer(self, optimizer):
        self.optimizer = optimizer

    def add_loss_op(self, loss_op):
        self.loss_op = loss_op

    def reduce_lr(self):
        print("Reducing LR")
        for g in self.optimizer.param_groups:
            g['lr'] = g['lr'] / 2

    def run_epoch(self, train_iterator, val_iterator, epoch):
        train_losses = []
        val_accuracies = []
        losses = []

        # Reduce learning rate as number of epochs increase
        if (epoch == int(self.config.max_epochs / 3)) or (epoch == int(2 * self.config.max_epochs / 3)):
            self.reduce_lr()

        for i, batch in enumerate(train_iterator):
            self.optimizer.zero_grad()
            if torch.cuda.is_available():
                x = batch.text.cuda()
                y = (batch.label - 1).type(torch.cuda.LongTensor)
            else:
                x = batch.text
                y = (batch.label - 1).type(torch.LongTensor)
            y_pred = self.__call__(x)
            loss = self.loss_op(y_pred, y)
            loss.backward()
            losses.append(loss.data.cpu().numpy())
            self.optimizer.step()

            if i % 100 == 0:
                print("Iter: {}".format(i + 1))
                avg_train_loss = np.mean(losses)
                train_losses.append(avg_train_loss)
                print("\tAverage training loss: {:.5f}".format(avg_train_loss))
                losses = []

                # Evalute Accuracy on validation set
                val_accuracy = evaluate_model(self, val_iterator)
                print("\tVal Accuracy: {:.4f}".format(val_accuracy))
                self.train()

        return train_losses, val_accuracies

    def run_epoch_dp(self, train_iterator, val_iterator, epoch, clip, std_grad):
        train_losses = []
        val_accuracies = []
        losses = []

        # Reduce learning rate as number of epochs increase
        if (epoch == int(self.config.max_epochs / 3)) or (epoch == int(2 * self.config.max_epochs / 3)):
            self.reduce_lr()

        for i, batch in enumerate(train_iterator):
            self.optimizer.zero_grad()
            if torch.cuda.is_available():
                x = batch.text.cuda()
                y = (batch.label - 1).type(torch.cuda.LongTensor)
            else:
                x = batch.text
                y = (batch.label - 1).type(torch.LongTensor)
            y_pred = self.__call__(x)
            loss = self.loss_op(y_pred, y)

            saved_var = dict()
            for tensor_name, tensor in self.named_parameters():
                if tensor_name == "embeddings.weight": continue
                saved_var[tensor_name] = torch.zeros_like(tensor)

            # print(saved_var.keys())

            for j in loss:
                j.backward(retain_graph=True)
                # param_norm_before = compute_norm(model, norm_type=2)
                torch.nn.utils.clip_grad_norm_(self.parameters(), clip, norm_type=2)
                # param_norm_after = compute_norm(model, norm_type=2)
                # total_diff += np.abs(param_norm_after - param_norm_before)
                # print(self.named_parameters())
                for tensor_name, tensor in self.named_parameters():
                    if tensor_name == "embeddings.weight": continue
                    new_grad = tensor.grad
                    saved_var[tensor_name].add_(new_grad)
                self.optimizer.zero_grad()

            for tensor_name, tensor in self.named_parameters():
                if tensor_name == "embeddings.weight": continue
                # saved_var[tensor_name].add_(tensor.grad)
                noise = std_grad * torch.randn_like(tensor.grad)
                saved_var[tensor_name].add_(noise)
                tensor.grad = saved_var[tensor_name] / loss.shape[0]

            # loss.backward()
            losses.append(loss.data.mean().cpu().numpy())
            self.optimizer.step()

            if i % 100 == 0:
                print("Iter: {}".format(i + 1))
                avg_train_loss = np.mean(losses)
                train_losses.append(avg_train_loss)
                print("\tAverage training loss: {:.5f}".format(avg_train_loss))
                losses = []

                # Evalute Accuracy on validation set
                val_accuracy = evaluate_model(self, val_iterator)
                print("\tVal Accuracy: {:.4f}".format(val_accuracy))
                self.train()

        return train_losses, val_accuracies


#### util.py
class Dataset(object):
    def __init__(self, config):
        self.config = config
        self.train_iterator = None
        self.test_iterator = None
        self.val_iterator = None
        self.vocab = []
        self.word_embeddings = {}

    def parse_label(self, label):
        '''
        Get the actual labels from label string
        Input:
            label (string) : labels of the form '__label__2'
        Returns:
            label (int) : integer value corresponding to label string
        '''
        return int(label.strip()[-1])

    def get_pandas_df(self, filename):
        '''
        Load the data into Pandas.DataFrame object
        This will be used to convert data to torchtext object
        '''
        with open(filename, 'r') as datafile:
            data = [line.strip().split(',', maxsplit=1) for line in datafile]
            data_text = list(map(lambda x: x[1], data))
            data_label = list(map(lambda x: self.parse_label(x[0]), data))

        full_df = pd.DataFrame({"text": data_text, "label": data_label})
        return full_df

    def load_data(self, train_file, test_file, val_file=None):
        '''
        Loads the data from files
        Sets up iterators for training, validation and test data
        Also create vocabulary and word embeddings based on the data

        Inputs:
            w2v_file (String): absolute path to file containing word embeddings (GloVe/Word2Vec)
            train_file (String): absolute path to training file
            test_file (String): absolute path to test file
            val_file (String): absolute path to validation file
        '''

        NLP = spacy.load('en')
        tokenizer = lambda sent: [x.text for x in NLP.tokenizer(sent) if x.text != " "]

        # Creating Field for data
        TEXT = data.Field(sequential=True, tokenize=tokenizer, lower=True, fix_length=self.config.max_sen_len)
        LABEL = data.Field(sequential=False, use_vocab=False)
        datafields = [("text", TEXT), ("label", LABEL)]

        # Load data from pd.DataFrame into torchtext.data.Dataset
        train_df = self.get_pandas_df(train_file)
        train_examples = [data.Example.fromlist(i, datafields) for i in train_df.values.tolist()]
        train_data = data.Dataset(train_examples, datafields)

        test_df = self.get_pandas_df(test_file)
        test_examples = [data.Example.fromlist(i, datafields) for i in test_df.values.tolist()]
        test_data = data.Dataset(test_examples, datafields)

        # If validation file exists, load it. Otherwise get validation data from training data
        if val_file:
            val_df = self.get_pandas_df(val_file)
            val_examples = [data.Example.fromlist(i, datafields) for i in val_df.values.tolist()]
            val_data = data.Dataset(val_examples, datafields)
        else:
            train_data, val_data = train_data.split(split_ratio=0.8)

        #TEXT.build_vocab(train_data, vectors=Vectors(w2v_file))
        TEXT.build_vocab(train_data, vectors="glove.6B.100d")

        self.word_embeddings = TEXT.vocab.vectors
        self.vocab = TEXT.vocab

        self.train_iterator = data.BucketIterator(
            (train_data),
            batch_size=self.config.batch_size,
            sort_key=lambda x: len(x.text),
            repeat=False,
            shuffle=True)

        self.val_iterator, self.test_iterator = data.BucketIterator.splits(
            (val_data, test_data),
            batch_size=self.config.batch_size,
            sort_key=lambda x: len(x.text),
            repeat=False,
            shuffle=False)

        print("Loaded {} training examples".format(len(train_data)))
        print("Loaded {} test examples".format(len(test_data)))
        print("Loaded {} validation examples".format(len(val_data)))


def evaluate_model(model, iterator):
    all_preds = []
    all_y = []
    for idx, batch in enumerate(iterator):
        if torch.cuda.is_available():
            x = batch.text.cuda()
        else:
            x = batch.text
        y_pred = model(x)
        predicted = torch.max(y_pred.cpu().data, 1)[1] + 1
        all_preds.extend(predicted.numpy())
        all_y.extend(batch.label.numpy())
    score = accuracy_score(all_y, np.array(all_preds).flatten())
    return score


if __name__ == '__main__':

    # python train.py [experiment_name] [noise_std]
    '''   
    The sample configuration using tensorflow privacy is:
    python compute_dp_sgd_privacy.py --N=96000 --batch_size=128 --noise_multiplier=0.621 --epochs=50 --delta=1e-5

    You have to run the code with:
    python main_text_classification.py [experiment_name] [noise_std] [clip]
    '''
    train_file = 'data/ag_news.train'
    test_file = 'data/ag_news.test'

    print(sys.argv)
    experiment_name = 'non-dp'
    isDP = False

    config = Config()
    if len(sys.argv) < 2:
        print("BiLSTM model")
        experiment_name = 'non-dp'
    elif len(sys.argv) > 2:
        if sys.argv[1] == 'dp':
            print("BiLSTM DP model")
            experiment_name = 'dp'
            noise_multiplier = float(sys.argv[2])
            clip = float(sys.argv[3])
            isDP = True
        elif sys.argv[1] == 'sidp':
            print("BiLSTM SIDP model")
            experiment_name = 'sidp'
            noise_multiplier = float(sys.argv[2])
            clip = float(sys.argv[3])
            isDP = True
            noise_std = float(sys.argv[2])
    else:
        print("Please run the code: python train.py [experiment_name] [noise_std] e.g")
        print("python main_text_classification.py sidp 1.082 7")

    dataset = Dataset(config)
    dataset.load_data(train_file, test_file)

    # Create Model with specified optimizer and loss function
    ##############################################################
    if experiment_name in ['sidp']:
        model = TextSIRNN(config, len(dataset.vocab), dataset.word_embeddings, noise_std)
    else:
        model = TextRNN(config, len(dataset.vocab), dataset.word_embeddings)
    if torch.cuda.is_available():
        model.cuda()
    model.train()

    if isDP:
        optimizer = optim.SGD(model.parameters(), lr=config.lr * 100)
        # optimizer = optim.Adam(model.parameters(), lr=config.lr)
        criterion = nn.NLLLoss(reduction='none')

    else:
        optimizer = optim.Adam(model.parameters(), lr=config.lr)
        criterion = nn.NLLLoss()

    model.add_optimizer(optimizer)
    model.add_loss_op(criterion)
    ##############################################################

    train_losses = []
    val_accuracies = []

    for i in range(config.max_epochs):
        print("Epoch: {}".format(i))
        if experiment_name in ['dp']:
            noise_std = clip * noise_multiplier
            train_loss, val_accuracy = model.run_epoch_dp(dataset.train_iterator, dataset.val_iterator, i, clip,
                                                          noise_std)
        elif experiment_name in ['sidp']:
            sgd_lr = config.lr * 100
            train_loss, val_accuracy = model.run_epoch_sidp(dataset.train_iterator, dataset.val_iterator, i, clip,
                                                            noise_std, sgd_lr, config.batch_size)
            # train_loss, val_accuracy = model.run_epoch(dataset.train_iterator, dataset.val_iterator, i)
        else:
            train_loss, val_accuracy = model.run_epoch(dataset.train_iterator, dataset.val_iterator, i)
        train_losses.append(train_loss)
        val_accuracies.append(val_accuracy)

    train_acc = evaluate_model(model, dataset.train_iterator)
    val_acc = evaluate_model(model, dataset.val_iterator)
    test_acc = evaluate_model(model, dataset.test_iterator)

    print('Final Training Accuracy: {:.4f}'.format(train_acc))
    print('Final Validation Accuracy: {:.4f}'.format(val_acc))
    print('Final Test Accuracy: {:.4f}'.format(test_acc))


