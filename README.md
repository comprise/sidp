SIDP is a tool to train differentially private models with a low privacy budget while preserving high performance/accuracy.
* The currrent version supports the LSTM, CNN and Linear models
----

## Requirements
Python 2.7 or 3 with following packages:
* pytorch
* tensorflow_privacy (https://github.com/tensorflow/privacy)

The examples we provided requires other packages like
* torchvision
* torchtext
* pandas
* spacy   
* scikit-learn

## Usage
### Obtain the noise multiplier corresponding to your choice of epsilon (privacy loss): Run the code in this path

https://github.com/tensorflow/privacy/blob/master/tensorflow_privacy/privacy/analysis/compute_dp_sgd_privacy.py 

For example, 

> compute_dp_sgd_privacy.py --N=60000 --batch_size=256\
                          --noise_multiplier=1.1 \
                          --epochs=60 --delta=1e-5


### For the image classification on MNIST data, please run:
> python main_mnist.py [experiment_name] [noise_multiplier] [clip]

* experiment_name is one of the following "non-dp", "dp" or "sidp"
* noise_multiplier is the amount of noise i.e sigma to be added to the gradient
* clip is the clipping factor i.e C to preserve the sensitivity of differential privacy

### For the text classification on AG News corpus data, please run:
> python  main_text_classification.py [experiment_name] [noise_multiplier] [clip]
